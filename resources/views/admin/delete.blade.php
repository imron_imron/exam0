@extends('layout.app')
@section('content')
<title>Delete Unit</title>
<div class="container">
    <div class="row">
        </br>
        <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
        
                <div class="panel-heading">Delete this unit?</div>

                <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                    @foreach($prod as $prod)
                        Nama Kavling    : {{$prod->rumah_kavling}}
                        </br>
                        Nama Blok       : {{$prod->rumah_blok}}
                        </br>
                        Nomor Rumah     : {{$prod->rumah_nomor}}
                    @endforeach
                    </div>
                    <div class="col-md-12">
                    <form method="POST" action="{{ route('deleteUnitrumah') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="rumah_id" value="{{$prod->rumah_id}}" />
                    <input type="submit" value="Delete" class="btn btn-danger" > <a button class="btn btn-primary" href="{{ route('ListUnit') }}">Back </a></button>
                    {{--  <a class="btn btn-danger" input type="submit" value="Yes">Delete</a> <button class="btn btn-primary" href="unit/units">Back</button>  --}}
                    </form>
                    {{--  <a class="btn btn-danger" href="{{ route('ListUnit') }}">Back</a> </button>  --}}
                    
                    </div>

                </div>
        </div>
    </div>
</div>
@endsection