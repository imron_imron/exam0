@extends('layout.app')
@section('content')
<title>Edit Unit</title>
<div class="container">
    <div class="row">
    @foreach($prod as $prod)
        {{--  <div class="col-12">  --}}
        </br>
        <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
        <div class="panel-heading">Edit detail Rumah</div>
             
<form class="form-horizontal" method="POST" action="{{route('EditUnitrumah')}}">
                {{csrf_field()}}
                    
                        <div class="form-group">
                            <label for="product_name" class="col-sm-2 control-label">Kavling</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" name="kavling" id="kavling" placeholder="Cth:Margonda" value="{{$prod->rumah_kavling}}">
                             
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="product_image" class="col-sm-2 control-label">blok</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" name="blok" id="blok" placeholder="Cth:blok A" value="{{$prod->rumah_blok}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="product_description" class="col-sm-2 control-label">nomor rumah</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" name="nomor" id="nomor" placeholder="Cth:50" value="{{$prod->rumah_nomor}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-sm-2 control-label">Harga</label>
                            <div class="col-sm-10">
                            <input type="number" class="form-control" name="harga" id="harga" placeholder="Cth:400000000" value="{{$prod->rumah_harga}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="product_category" class="col-sm-2 control-label">luas tanah</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" name="luastanah" id="luastanah" placeholder="Cth:100" value="{{$prod->rumah_luastanah}}">
                            </div>
                            </div>
                        <div class="form-group">
                            <label for="product_type" class="col-sm-2 control-label">Luas bangunan</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" name="luasbangunan" id="luasbangunan" placeholder="Cth:100" value="{{$prod->rumah_luasbangunan}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="product_type" class="col-sm-2 control-label">Customer ID (1 or 2)</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" name="customerno" id="customerno" placeholder="Cth:1 or 2" value="{{$prod->customer_id}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                            <input type="hidden" name="rumah_id" value="{{$prod->rumah_id}}" />
                            <button type="submit" class="btn btn-warning">Update</button>
                            {{--  <button type="submit" class="btn btn-primary">Back</button>  --}}
                            <a class="btn btn-danger" href="{{ route('ListUnit') }}">Back</a> </button>
                            </div>
                        </div>
            </form>
        </div>
        @endforeach
    </div>
</div>
    @endsection