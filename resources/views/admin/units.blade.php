@extends('layout.app')
@section('content')
<title>List Unit</title>
<div class="container">
    <div class="row">
        </br>
        <div class="col-md-10 col-md-offset-1">
        <table class="table">
        <tr>
        <td><a class="btn btn-success" href="/admin">Create new unit</a> </td>
        </tr>
        </table>
        <div class="panel panel-default"> 
        <div class="panel-heading">Unit Rumah List </div>
        <div class="panel-body">
        
        <table class="table table-striped">
            <tr>
                <th>No</th>
                <th>Kavling</th>
                <th>Blok</th>
                <th>No Rumah</th>
                <th>Harga Rumah</th>
                <th>Luas Tanah (m2)</th>
                <th>Luas Bangunan(m2)</th>
                <th>Customer No</th>
                <th>Action</th>
            </tr>
            @foreach($data as $prod)
             <tr>
                <td>{{$prod->rumah_id}}</td>
                <td>{{$prod->rumah_kavling}}</td>
                <td>{{$prod->rumah_blok}}</td>
                <td>{{$prod->rumah_nomor}}</td>
                <td>{{$prod->rumah_harga}}</td>
                <td>{{$prod->rumah_luastanah}}</td>
                <td>{{$prod->rumah_luasbangunan}}</td>
                <td>{{$prod->customer_id}}</td>
                <td><a class="btn btn-danger" href="delete/{{$prod->rumah_id}}">Delete</a> <a class="btn btn-primary" href="unit/{{$prod->rumah_id}}">Edit</button></td>
                
                
                
            </tr>

            @endforeach
            </table>
            </div>
            </div>
@endsection
