@extends('layout.app')
@section('content')
<title>Create new Unit</title>
<div class="container">
    <div class="row">
    </br>
        <div class="col-md-12 ">
            <form class="form-horizontal" method="POST" action="{{route('CreateUnit')}}">
                {{csrf_field()}}

                        <div class="form-group">
                            <label for="product_name" class="col-sm-2 control-label">Kavling</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" name="kavling" id="kavling" placeholder="Cth:Margonda">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="product_image" class="col-sm-2 control-label">blok</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" name="blok" id="blok" placeholder="Cth:blok A">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="product_description" class="col-sm-2 control-label">nomor rumah</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" name="nomor" id="nomor" placeholder="Cth:50">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-sm-2 control-label">Harga</label>
                            <div class="col-sm-10">
                            <input type="number" class="form-control" name="harga" id="harga" placeholder="Cth:400000000">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="product_category" class="col-sm-2 control-label">luas tanah</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" name="luastanah" id="luastanah" placeholder="Cth:100">
                            </div>
                            </div>
                        <div class="form-group">
                            <label for="product_type" class="col-sm-2 control-label">Luas bangunan</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" name="luasbangunan" id="luasbangunan" placeholder="Cth:100">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="product_type" class="col-sm-2 control-label">Customer ID</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" name="customerno" id="customerno" placeholder="Cth:1 or 2">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
            </form>
        </div>
    </div>
</div>
@endsection
