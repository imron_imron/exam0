<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\UnitRumah;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
       return view('admin.index');
    }

    public function listUnit()
    {
        $data = UnitRumah::all();
        return view('admin.units')->with('data',$data);
    }

    public function CreateUnit(Request $request)
    {
       
       $prod= new UnitRumah;
       $prod->rumah_kavling=$request->kavling;
       $prod->rumah_blok=$request->blok;
       $prod->rumah_nomor=$request->nomor;
       $prod->rumah_harga=$request->harga;
       $prod->rumah_luastanah=$request->luastanah;
       $prod->rumah_luasbangunan=$request->luasbangunan;
       $prod->customer_id=$request->customerno;
    
       $prod->save();
       echo "Unit created successfully";

       return redirect('unit/units');
    }

    // public function deleteUnit(Request $request)
    // {
       
    //    $prod= new UnitRumah;
    //   $prod->rumah_kavling=$request->kavling;
    //    $deleted=UnitRumah::delete('delete from unit where rumah_kavling= ?', [$request->kavling]);
    //    $commit=UnitRumah::statement('commit');
    //    echo "Unit deleted successfully";

    //    return redirect('admin');
    // }

    // public function deleteUnit(Request $kavling) {
        
    //     DB::delete('delete from unit where rumah_kavling = ?',[$kavling]);
    //     DB::table('unit')->where('rumah_kavling', '?', 100)->delete();
    //     DB::statement('commit');
    //     echo "Record deleted successfully.<br/>";
    //     return view('admin.units');
    //  }

    public function deleteUnit(Request $request)
    {
        //$data = UnitRumah::find($request->rumah_id);
        $data=DB::select('select * from unitrumah where rumah_id = '.$request->rumah_id);
        return view('admin.delete',['prod' => $data]);
    }

    public function confirmDelete(Request $request)
    {
        $data=DB::table('unitrumah')->where('rumah_id','=',$request->rumah_id)->delete();
        //UnitRumah::destroy((int)$request->rumah_id);
        echo "unit deleted successfully";
        return redirect('unit/units');
    }

    public function EditUnit(Request $request)
    {
        //$data = UnitRumah::find($request->rumah_id);
        $data=DB::select('select * from unitrumah where rumah_id = '.$request->rumah_id);
        return view('admin.edit',['prod' => $data]);
    }

    public function confirmEdit(Request $request)
    {
        // $data=DB::table('unitrumah')->where('rumah_id','=',$request->rumah_id)->update();
        //UnitRumah::destroy((int)$request->rumah_id);
        $data=DB::table('unitrumah')->where('rumah_id','=',$request->rumah_id)
        ->update([
            'rumah_kavling' =>$request->kavling,
            'rumah_blok' =>$request->blok,
            'rumah_nomor' =>$request->nomor,
            'rumah_harga' =>$request->harga,
            'rumah_luastanah' =>$request->luastanah,
            'rumah_luasbangunan' =>$request->luasbangunan,
            'customer_id' =>$request->customerno
        ]);
        echo "unit updated successfully";
        return redirect('unit/units');
    }   


}
