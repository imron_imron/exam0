<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitrumahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unitrumah', function (Blueprint $table) {
            $table->increments('rumah_id');
            $table->timestamps();
            $table->string('rumah_kavling');
            $table->string('rumah_blok');
            $table->string('rumah_nomor');
            $table->integer('rumah_harga');
            $table->string('rumah_luastanah');
            $table->string('rumah_luasbangunan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unitrumah');
    }
}
