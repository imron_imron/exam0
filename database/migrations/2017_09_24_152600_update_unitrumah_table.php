<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUnitrumahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         //add column table unit and add foreign key
        Schema::table('unitrumah', function (Blueprint $table) {
            $table->integer('customer_id')->unsigned();
            $table->foreign('customer_id')
                    ->references('customer_id')->on('customers')
                    ->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //drop colum table unit and remove foreign key
        Schema::table('unitrumah', function (Blueprint $table) {
            $table->dropColumn('customer_id');
            $table->dropForeign('unitrumah_customer_id_foreign');
        });
    }
}
