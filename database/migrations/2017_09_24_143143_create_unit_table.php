<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('rumah_kavling');
            $table->string('rumah_blok');
            $table->string('rumah_nomor');
            $table->integer('rumah_harga');
            $table->string('rumah_luastanah');
            $table->string('rumah_luasbangunan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit');
    }
}
