<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin', function () {
    return view('admin.index');
});

Route::prefix('unit')->group(function () {
    Route::get('/units','ProductController@listUnit')->name('ListUnit');
    Route::post('/create_unit', 'ProductController@CreateUnit')->name('CreateUnit');
    Route::get('delete/{rumah_id}','ProductController@deleteUnit')->name('deleteUnit');
    Route::post('/delete_unit','ProductController@confirmDelete')->name('deleteUnitrumah');
    Route::get('unit/{rumah_id}', 'ProductController@EditUnit')->name('EditUnit');
    Route::post('/edit_unit','ProductController@confirmEdit')->name('EditUnitrumah');
    //Route::get('admin', 'ProductController@index');
});